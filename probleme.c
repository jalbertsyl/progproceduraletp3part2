/*!
\file probleme.c
\autor Jalbert Sylvain
\version 1
\date 18 octobre 2019
\brief un programme qui propose via un menu des actions et les execute si l'utilisateur les choisis
*/

#include <stdio.h>
#include <stdlib.h>

/*!
\fn void sapin ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 18 octobre 2019
\brief une procedure qui dessine un sapin
\param n le nombre d'étage du sapin
*/
void sapin (int n){
  //DECLARATION DES VARIABLES
  char char_caractere; //le caractère courant qui sera dessiné
  int int_borne; //la borne qui sera utiliser pour delimiter les * du sapin

  //DEFINITION DES VARIABLES
  int_borne = n; //la borne prend la valeur de n

  //DESSIN DU HAUT DE L'ARBRE
  for(int i = 0 ; i<=n ; i++){ //i représente les ligne, on parcour donc chaque ligne
    for(int j = 0 ; j<n*2-int_borne ; j++){ //j represente les colonnes, on parcour donc chaque colonnes (chaque caractère dans la ligne)
      if(j<int_borne) printf(" "); //si on se situe avant la borne, on met un espace
      else if(j>int_borne) printf("*"); //sinon on met une etoile car un est dans les branches du sapin (*)
    }
    int_borne--; //la borne est décrémenté
    printf("\n"); //a la fin de chaque ligne, on fait un retour à la ligne
  }

  //DESSIN DU TRONC
  for(int i = 0 ; i<3 ; i++){ //on dessine 3 lignes
    for(int j = 0 ; j<n-2; j++) printf(" "); //on met des espace jusqu'à la moitié du sapin - 2 caractères
    printf("@@@\n"); //on dessine la ligne courante du tronc
  }
}

/*!
\fn int maxFact ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief une fonction qui cherche le nombre n pour lequel n! ≤ k
\param k l'entier superieur ou egal à la factorielle cherché
\return int_n la factorielle inferieur ou egale à l'entier
*/
int maxFact (int k){
  //DECLARATION DES VARIABLES
  int int_n; //la factorielle recherché
  int int_res; //resultat de la factorielle recherché

  //DEFINITION DES VARIABLES
  int_n = 1; //la factorielle de 1 est 1, donc pour int_n = 1, le resultat est int_res = 1
  int_res = 1;

  //CALCUL DE LA FACTORIELLE QUI NE DEPASSE PAS k
  while(int_res <= k){
    int_n++; //incrementation de l'indice de factorielle
    int_res = int_res * int_n; //calcul de la factorielle au rang int_n
  }
  int_n--; //on a dépassé k, donc on décrémente int_n pour revenir à la derniere position qui remplicait la condition

  //RENVOYER LE RANG DE LA FACTORIELLE
  return int_n; //on retourn le rang n de la factorielle pour lequel !n ≤ k
}

/*!
\fn void estArmstrong ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief une procedure qui dit si nbr est un nombre d'Armstrong ou non
\param nbr le potentiel nombre d'Armstrong
*/
void estArmstrong (int n){
  //DECLARATION DES VARIABLES
  int int_resultat; //Le resultat de la somme des cubes des chiffres qui le composent
  int int_chiffre; //Le chiffre courant qui servira pour faire la somme
  int int_nbr; //le nombre qui sera une copie de n et qui servira pour calculer la somme des cubes des chiffre que compose n

  //DEFINITION DES VARIABLES
  int_nbr = n; //initialisation de la variable qui sera modifié durant le calcul
  int_resultat = 0; //initialisation du resultat

  //CALCUL
  while(int_nbr!=0){ //tant que le nombre est differant de 0
    int_chiffre = int_nbr % 10; //on recupère le chiffre des unitées
    int_resultat += int_chiffre*int_chiffre*int_chiffre; //on ajoute le cube de ce chiffre à la somme
    int_nbr = int_nbr / 10; //on enlève au nombre le chiffre que l'on vient de traiter
  }

  //AFFICHER LE RESULTAT
  if(int_resultat==n) printf("Le nombre %d est un nombre d'Armstrong !\n", n); //si le resultat du calcul est égal au nombre de départ, alors on affiche que le nombre est un nombre d'Armstrong
  else printf("Le nombre %d n'est pas un nombre d'Armstrong...\n", n); //sinon on affiche qu'il ne l'est pas
}

/*!
\fn void binaire ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 22 octobre 2019
\brief une procedure qui affiche un entier donné en binaire
\param n l'entier qui sera converti
*/
void binaire (int n){
  //DECLARATION DES VARIABLES
  int int_tab[32]; //tableau qui va contenir le nombre binaire inversé
  int int_i; //compteur de bits

  //INITIALISATION DES VARIABLES
  int_i = 0;

  //affichage du debut du message de résultat
  printf("%d en binaire est : ", n);

  //CONVERTION EN BINAIRE
  while(n/2 > 0){
   int_tab[int_i] = n%2; //mettre dans le tableau le reste de la division euclidienne de n par 2 (correspond au bit courant)
   n = n/2; //on divise n par deux (le resultat sera tranché)
   int_i++; //on incremente int_i pour passer à la case suivante dans le tableau
  }
  int_tab[int_i] = n%2; //on met le dernier bit dans le tableau

  //AFFICHAGE DU TABLEAU
  for(int_i ; int_i>=0 ; int_i--){ //parcour du tableau depuis la derniere case à la premiere
    printf("%d", int_tab[int_i]); //affichage de la case int_i du tableau
  }
  printf("\n"); //faire un retour à la ligne
}


/*!
\fn int fact ( int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 23 octobre 2019
\brief une fonction qui calcul la factorielle de n
\param n le nombre pour lequel nous calculerons sa factorielle
\return la factorielle de n
*/
int fact(int n){
  //DECLARATION DES VARIABLES
  int int_res; //le resultat de la factorielle

  //INITIALISATION DES VARIABLES
  int_res = 1;

  //CALCUL DE LA FACTORIELLE
  for(n ; n>1 ; n--){ //parcour de n à 2
    int_res = int_res * n; //le resutat se multiplie à n
  }

  //RETOURNER LA FACTORIELLE
  return int_res;

}

/*!
\fn int coefBin ( int , int )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 23 octobre 2019
\brief une fonction qui calcul et retourne le coeficient biniomal de p parmi n
\param n
\param p
\return le coefficient biniomal de p parmi n
*/
int coefBin(int n, int p){
  //CALCUL DU COEFICIENT
  return fact(n)/(fact(n-p)*fact(p));

}

/*!
\fn int main ( int argc, char∗∗ argv )
\author Jalbert Sylvain
\version 0.1 Premier jet
\date 18 octobre 2019
\brief la fonction principale qui va proposer à l'utilisateur un menu et va appeler les fonctions/procedures qui correspondent au choix de l'utilisateur
\param argc nombre d’arguments en entree
\param argv valeur des arguments en entree
\return 0 si tout c'est bien passé
*/
int main (int argc, char** argv){
  //DECLARATION DES VARIABLES
  int int_choix; //Le choix de l'utilisateur
  int int_param; //paramètre de la méthode qui sera apelé
  int int_param2; //second paramètre de la méthode coefBin

  //AFFICHAGE DU MENU
  printf("1. Affichage d’un sapin\n2. Rang de la factorielle\n3. Nombre d’Armstrong\n4. Affichage binaire\n5. Coefficients binômiaux\n\n");

  //CHOIX DE L'UTILISATEUR
  printf("Veuillez saisir un entier pour choisir: "); //Demmande de saisie
  while(!scanf("%d",&int_choix)){ //Verifier que la saisie est bien un entier
    printf("La saisie n'est pas un entier ! Veuillez saisir un entier pour choisir: "); //Message d'erreur de saisie
  }

  //APPEL DE LA PROCEDURE/FONCTION QUI CORRESOND AU CHOIX
  switch (int_choix) {
    case 1: //l'utilisateur a choisi d'afficher un sapin qui a un nombre d'étage saisie
      printf("Veuillez saisir le nombre d'étage du sapin : ");
      while(!scanf("%d",&int_param)){ //Verifier que la saisie est bien un entier
        printf("La saisie n'est pas un entier ! Veuillez saisir un entier: "); //Message d'erreur de saisie
      }
      sapin((int_param<0 ? 0-int_param : int_param)); //appeler la procedure avec un nombre d'étage positif
      break;
    case 2: //l'utilisateur a choisi de trouver la factorielleinferieur à un nombre saisie
      printf("Veuillez saisir l'entier à ne pas dépasser : ");
      while(!scanf("%d",&int_param)){ //Verifier que la saisie est bien un entier
        printf("La saisie n'est pas un entier ! Veuillez saisir un entier: "); //Message d'erreur de saisie
      }
      printf("La factorielle qui ne dépasse pas %d est %d!\n", int_param, maxFact((int_param<0 ? 0-int_param : int_param))); //affichage du resultat de la fonction factorielle (en lui passant un entier positif)
      break;
    case 3: //l'utilisateur a choisi de tester si un nombre est un nombre d'Armstrong
      printf("Veuillez saisir le nombre à tester : ");
      while(!scanf("%d",&int_param)){ //Verifier que la saisie est bien un entier
        printf("La saisie n'est pas un entier ! Veuillez saisir un entier: "); //Message d'erreur de saisie
      }
      estArmstrong(int_param); //appeler la procedure avec le nombre à tester
      break;
    case 4: //l'utilisateur a choisi de convertir un entier saisie en binaire
      printf("Veuillez saisir un nombre à convertir en binaire : ");
      while(!scanf("%d",&int_param)){ //Verifier que la saisie est bien un entier
        printf("La saisie n'est pas un entier ! Veuillez saisir un entier: "); //Message d'erreur de saisie
      }
      binaire((int_param<0 ? 0-int_param : int_param)); //appeler la procedure avec le nombre à tester positif
      break;
    case 5: //l'utilisateur a choisi de calculer le coefficient binomial entre deux entier saisie
      printf("Veuillez saisir n : ");
      while(!scanf("%d",&int_param)){ //Verifier que la saisie est bien un entier
        printf("La saisie n'est pas un entier ! Veuillez saisir un entier: "); //Message d'erreur de saisie
      }
      printf("Veuillez saisir p : ");
      while(!scanf("%d",&int_param2)){ //Verifier que la saisie est bien un entier
        printf("La saisie n'est pas un entier ! Veuillez saisir un entier: "); //Message d'erreur de saisie
      }
      printf("Le coeficient biniomal de %d parmis %d est : %d\n", int_param2, int_param,
              coefBin((int_param<0 ? 0-int_param : int_param),(int_param2<0 ? 0-int_param2 : int_param2))); //affichage du coeficient biniomal de p parmi n
      break;
    default: //le choix est differant de ceux proposé. L'utilisateur veux alors quitter le programme
      printf("Vous n'avez pas selectionné de choix. Au revoir !\n\n"); //Message indicant à l'utilisateur que le programme va s'arreter
      exit(-1); //On quitte le programme
      break;
  }

  //AFFICHAGE DE FIN DE PROGRAMME
  printf("Le programme est désormais terminé. Au revoir !\n"); //dit à l'utilisateur que tout c'est bien passé

  /* Fin du programme, Il se termine normalement, et donc retourne 0 */
  return 0;
}
