# Readme.md
Ce fichier readme.md contient les réponses des exercices du TP ainsi que les instructions pour compiler et executer le programme du TP.

## Documentation
La documentation est faite avec doxygen. Pour l'observée executer la comande suivante :

    doxygen Doxyfile

Puis visualiser dans votre navigateur le fichier ./html/index.html

## Programme
### Compiler
Pour compiler, il faut saisir dans le terminal linux, à la racine du projet, la commande suivante :

    gcc ./probleme.c -o ./probleme

### Executer
Pour executer, il suffi de saisir dans le terminal linux, à la racine du projet, la commande suivante :

    ./probleme

## Coefficients binomiaux
Pour cet exercices la dificulté était la redondance du calcul de factorielle. J'ai alors créé une fonction nomé fact qui renvoie la factorielle de l'entier passé en paramettre.
